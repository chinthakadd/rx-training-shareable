package com.virtusa.training.rx.aggregator.client;

import com.virtusa.training.rx.aggregator.model.AccountDto;
import com.virtusa.training.rx.aggregator.model.TransactionDto;
import io.reactivex.Observable;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.UUID;

@Component
public class AccountClient {

    public Observable<AccountDto> getAccounts(int customerId) {
        return Observable.range(0, customerId)
                .map(integer ->
                        new AccountDto(integer, UUID.randomUUID().toString(), new Random().nextDouble())
                );
    }

    public Observable<TransactionDto> getTransactions(int customerId, int accountId) {
        return Observable
                .range(0, 50)
                .map(integer -> new TransactionDto(
                        String.format("Customer=%d,Account=%d,Transaction=%d",
                                customerId, accountId, integer), new Random().nextDouble()));
    }
}
