package com.virtusa.training.rx.aggregator.model;

public class TransactionDto {
    private String description;
    private double amount;

    public TransactionDto() {
    }

    public TransactionDto(String description, double amount) {
        this.description = description;
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
