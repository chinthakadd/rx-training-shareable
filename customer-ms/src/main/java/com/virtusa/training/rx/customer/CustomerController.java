package com.virtusa.training.rx.customer;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * GET a Customer By Customer ID.
 * Customer should contain a unique ID, first name, last name
 */
@RestController
@RequestMapping("/customers")
public class CustomerController {
}
